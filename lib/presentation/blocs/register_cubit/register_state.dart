part of 'register_cubit.dart';

enum FormStatus { invalid, valid, validating, posting }

class RegisterFormState extends Equatable {
  final FormStatus formStatus;
  // final String username;
  final Username username;

  final String email;
  // final String password;
  final Password password;

  final bool isValid;

  const RegisterFormState({
    this.formStatus = FormStatus.invalid,
    // this.username = '',
    this.username = const Username.pure(),
    this.email = '',
    // this.password = '',
    this.password = const Password.pure(),
    this.isValid = false,
  });

  RegisterFormState copyWith({
    FormStatus? formStatus,
    // String? username,
    bool? isValid,
    Username? username,
    String? email,
    // String? password,
    Password? password,
  }) {
    return RegisterFormState(
      formStatus: formStatus ?? this.formStatus,
      username: username ?? this.username,
      email: email ?? this.email,
      password: password ?? this.password,
      isValid: isValid ?? this.isValid,
    );
  }

  @override
  List<Object> get props => [formStatus, username, email, password, isValid];
}
