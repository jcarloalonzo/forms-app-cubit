import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(const CounterState()) {
    // on<CounterEvent>((event, emit) {
    //   // TODO: implement event handler
    // });

    // on<CounterIncreased>(
    //   (event, emit) => emit(
    //     state.copyWith(
    //         counter: state.counter + event.value,
    //         transactionCount: state.transactionCount + 1),
    //   ),
    // );

    on<CounterIncreased>(
      (event, emit) => _onCounterIncreased(event, emit),
      // (event, emit) => _onCounterIncreased, // es igual
    );

    on<CounterReset>(
      // (event, emit) => _onCounterReset(event, emit),
      _onCounterReset, // es igual
    );
  }

  void _onCounterIncreased(CounterIncreased event, Emitter<CounterState> emit) {
    emit(
      state.copyWith(
          counter: state.counter + event.value,
          transactionCount: state.transactionCount + 1),
    );
  }

  void _onCounterReset(CounterReset event, Emitter<CounterState> emit) {
    emit(
      state.copyWith(counter: 0, transactionCount: state.transactionCount + 1),
    );
  }

//
//
  void increaseBy([int value = 1]) {
    add(CounterIncreased(value));
  }

  void resetCounter() {
    add(CounterReset());
  }

  //
  //
}
