import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forms_apps/presentation/blocs/register_cubit/register_cubit.dart';

import '../widgets/widgets.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Nuevo usuario')),
        body: BlocProvider(
            create: (context) => RegisterCubit(),
            child: const _RegisterView()));
  }
}

class _RegisterView extends StatelessWidget {
  const _RegisterView();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
        child: SingleChildScrollView(
          child: _RegisterForm(),
        ),
      ),
    );
  }
}

class _RegisterForm extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final registerCubit = context.watch<RegisterCubit>();

    return Form(
      key: _formKey,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          const FlutterLogo(size: 100),
          const SizedBox(height: 20),
          CustomTextFormField(
            label: 'Nombre de usuario',
            onChanged: (value) {
              registerCubit.usernameChanged(value);
            },
            validator: (value) {
              if (value == null || value.isEmpty) return 'campo requerido';
              if (value.trim().isEmpty) return 'campo requerido';
              if (value.length < 6) return 'más de 6 letras';
              return null;
            },
          ),
          const SizedBox(height: 10),
          CustomTextFormField(
            label: 'Correo electronico',
            onChanged: (value) {
              registerCubit.emailChanged(value);
            },
            validator: (value) {
              if (value == null || value.isEmpty) return 'campo requerido';
              if (value.trim().isEmpty) return 'campo requerido';

              final emailRegExp = RegExp(
                r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$',
              );

              if (!emailRegExp.hasMatch(value)) {
                return 'No tiene formato de correo';
              }

              return null;
            },
          ),
          const SizedBox(height: 10),
          CustomTextFormField(
            label: 'Contraseña',
            obscureText: true,
            onChanged: (value) {
              registerCubit.passwordChanged(value);
            },
            validator: (value) {
              if (value == null || value.isEmpty) return 'campo requerido';
              if (value.trim().isEmpty) return 'campo requerido';
              if (value.length < 6) return 'más de 6 letras';
              return null;
            },
          ),
          const SizedBox(height: 20),
          FilledButton.tonalIcon(
            onPressed: () {
              // final isValid = _formKey.currentState!.validate();
              // print(isValid);
              print(registerCubit.state.email);

              registerCubit.onSubmit();
            },
            label: const Text('Crear usuario'),
            icon: const Icon(Icons.save),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
